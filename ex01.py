
# a = int(input('1º numero: '))
# b = int(input('2º numero: '))

'''def mult(x, y):
    resultado = 0
    contador = 0
    
    while contador < y:
        resultado = resultado0 + x
        contador = contador + 1 

    return resultado'''
from functools import lru_cache

@lru_cache()
def multR(a, b):
    return 0 if a == 0 or b == 0 else - a + multR(a, b + 1) if b < 0 else a + multR(a, b - 1)

print(multR(3, -8))
print(multR(-3, 0))
print(multR(0, 4))
print(multR(3, 3))
print(multR(-2, -8))
