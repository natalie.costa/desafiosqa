def soma(a, b):
    #return soma(a ^ b, (a & b) >> 1) if b < 0 or a < 0 else a

    #return soma(a ^ b, (a & b) << 1) if b != 0 else a

    return a if b == 0 else soma(a ^ b, (a & b) << 1) if b > 0 else "teste"


print(soma(1, 1))
print(soma(2, 0)) 
print(soma(5, -4))
print(soma(10, 1))
print(soma(5, 9))
print(soma(-5, 3))
